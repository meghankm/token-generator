package com.meghan.accountservicesecurity.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableOAuth2Sso
public class TokenController {

    @Autowired
    private OAuth2ClientContext oAuth2ClientContext;
        
    @RequestMapping(value = "/getToken", method = RequestMethod.GET)
    public String getAccessToken(){
        OAuth2AccessToken oAuth2AccessToken = oAuth2ClientContext.getAccessToken();
        return oAuth2AccessToken.getValue();
    }    
}


